<?xml version="1.0" encoding="utf-8"?>
<resources>
    <declare-styleable name="Triangle">
        <attr name="triangleColor" format="color"/>
        <attr name="trianglePositionIsLeft" format="boolean" />
    </declare-styleable>

	<!-- Drawable used to render a geometric shape, with a gradient or a solid color. -->
	<declare-styleable name="SuperGradientDrawable">
		<!-- Indicates whether the drawable should intially be visible. -->
		<attr name="visible" format="boolean"/>
		<!-- Enables or disables dithering. -->
		<attr name="dither" format="boolean"/>
		<!-- Indicates what shape to fill with a gradient. -->
		<attr name="shape">
			<!-- Rectangle shape, with optional rounder corners. -->
			<enum name="rectangle" value="0" />
			<!-- Oval shape. -->
			<enum name="oval" value="1" />
			<!-- Line shape. -->
			<enum name="line" value="2" />
			<!-- Ring shape. -->
			<enum name="ring" value="3" />
		</attr>
		<!-- Inner radius of the ring expressed as a ratio of the ring's width. For instance,
		if innerRadiusRatio=9, then the inner radius equals the ring's width divided by 9.
		This value is ignored if innerRadius is defined. Default value is 9. -->
		<attr name="innerRadiusRatio" format="float" />
		<!-- Thickness of the ring expressed as a ratio of the ring's width. For instance,
		if thicknessRatio=3, then the thickness equals the ring's width divided by 3.
		This value is ignored if innerRadius is defined. Default value is 3. -->
		<attr name="thicknessRatio" format="float" />
		<!-- Inner radius of the ring. When defined, innerRadiusRatio is ignored. -->
		<attr name="innerRadius" format="dimension" />
		<!-- Thickness of the ring. When defined, thicknessRatio is ignored. -->
		<attr name="thickness" format="dimension" />
		<!-- Whether the drawable level value (see
		{@link android.graphics.drawable.Drawable#getLevel()}) is used to scale the shape.
		Scaling behavior depends on the shape type. For "ring", the angle is scaled from 0 to
		360. For all other types, there is no effect. The default value is true. -->
		<attr name="useLevel" format="dimension"/>
		<!-- If set, specifies the color to apply to the drawable as a tint. By default,
		no tint is applied. May be a color state list. -->
		<attr name="tint" format="color"/>
		<!-- When a tint color is set, specifies its Porter-Duff blending mode. The
		default value is src_in, which treats the drawable as an alpha mask. -->
		<attr name="tintMode" format="integer"/>
		<!-- Left optical inset.
		@hide Until optical insets are fully supported. -->
		<attr name="opticalInsetLeft" format="dimension"/>
		<!-- Top optical inset.
		@hide Until optical insets are fully supported. -->
		<attr name="opticalInsetTop" format="dimension"/>
		<!-- Right optical inset.
		@hide Until optical insets are fully supported. -->
		<attr name="opticalInsetRight" format="dimension"/>
		<!-- Bottom optical inset.
		@hide Until optical insets are fully supported. -->
		<attr name="opticalInsetBottom" format="dimension"/>
	</declare-styleable>

	<!-- Used to specify the size of the shape for SuperGradientDrawable. -->
	<declare-styleable name="SuperGradientDrawableSize">
		<!-- Width of the gradient shape. -->
		<attr name="width" format="dimension"/>
		<!-- Height of the gradient shape. -->
		<attr name="height" format="dimension"/>
	</declare-styleable>

	<!-- Used to describe the gradient used to fill the shape of a SuperGradientDrawable. -->
	<declare-styleable name="SuperGradientDrawableGradient">
		<!-- Start color of the gradient. -->
		<attr name="startColor" format="color" />
		<!-- Optional center color. For linear gradients, use centerX or centerY to place the center
		color. -->
		<attr name="centerColor" format="color" />
		<!-- End color of the gradient. -->
		<attr name="endColor" format="color" />
		<!-- Whether the drawable level value (see
		{@link android.graphics.drawable.Drawable#getLevel()}) is used to scale the gradient.
		Scaling behavior varies based on gradient type. For "linear", adjusts the ending
		position along the gradient's axis of orientation. For "radial", adjusts the outer
		radius. For "sweep", adjusts the ending angle. The default value is false. -->
		<attr name="useLevel" />
		<!-- Angle of the gradient, used only with linear gradient. Must be a multiple of 45 in the
		range [0, 315]. -->
		<attr name="angle" format="float" />
		<!-- Type of gradient. The default type is linear. -->
		<attr name="type">
			<!-- Linear gradient extending across the center point. -->
			<enum name="linear" value="0" />
			<!-- Radial gradient extending from the center point outward. -->
			<enum name="radial" value="1" />
			<!-- Sweep (or angular) gradient sweeping counter-clockwise around the center point. -->
			<enum name="sweep"  value="2" />
		</attr>
		<!-- X-position of the center point of the gradient within the shape as a fraction of the
		width. The default value is 0.5. -->
		<attr name="centerX" format="float|fraction" />
		<!-- Y-position of the center point of the gradient within the shape as a fraction of the
		height. The default value is 0.5. -->
		<attr name="centerY" format="float|fraction" />
		<!-- Radius of the gradient, used only with radial gradient. May be an explicit dimension
		or a fractional value relative to the shape's minimum dimension. -->
		<attr name="gradientRadius" format="float|fraction|dimension" />
	</declare-styleable>

	<!-- Used to fill the shape of SuperGradientDrawable with a solid color. -->
	<declare-styleable name="SuperGradientDrawableSolid">
		<!-- Solid color for the gradient shape. -->
		<attr name="color" format="color" />
	</declare-styleable>

	<!-- Used to describe the optional stroke of a SuperGradientDrawable. -->
	<declare-styleable name="SuperGradientDrawableStroke">
		<!-- Width of the gradient shape's stroke. -->
		<attr name="width" />
		<!-- Color of the gradient shape's stroke. -->
		<attr name="color" />
		<!-- Length of a dash in the stroke. -->
		<attr name="dashWidth" format="dimension" />
		<!-- Gap between dashes in the stroke. -->
		<attr name="dashGap" format="dimension" />
	</declare-styleable>

	<!-- Describes the corners for the rectangle shape of a SuperGradientDrawable.
	This can be used to render rounded corners. -->
	<declare-styleable name="DrawableCorners">
		<!-- Defines the radius of the four corners. -->
		<attr name="radius" format="dimension" />
		<!-- Radius of the top left corner. -->
		<attr name="topLeftRadius" format="dimension" />
		<!-- Radius of the top right corner. -->
		<attr name="topRightRadius" format="dimension" />
		<!-- Radius of the bottom left corner. -->
		<attr name="bottomLeftRadius" format="dimension" />
		<!-- Radius of the bottom right corner. -->
		<attr name="bottomRightRadius" format="dimension" />
	</declare-styleable>

	<!-- Used to specify the optional padding of a SuperGradientDrawable. -->
	<declare-styleable name="SuperGradientDrawablePadding">
		<!-- Amount of left padding inside the gradient shape. -->
		<attr name="left" format="dimension" />
		<!-- Amount of top padding inside the gradient shape. -->
		<attr name="top" format="dimension" />
		<!-- Amount of right padding inside the gradient shape. -->
		<attr name="right" format="dimension" />
		<!-- Amount of bottom padding inside the gradient shape. -->
		<attr name="bottom" format="dimension" />
	</declare-styleable>
</resources>


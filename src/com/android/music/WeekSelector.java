/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.music;

import android.app.*;
import android.media.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import com.keskin.music.*;
import java.util.*;

public class WeekSelector extends Activity
{
    Spinner mWeeks;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.weekpicker);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                                    WindowManager.LayoutParams.WRAP_CONTENT);

        mWeeks = (Spinner)findViewById(R.id.weeks);
		ArrayList al = new ArrayList<Integer>();
		for(String s : getResources().getStringArray(R.array.weeklist))
			al.add(s);
		ArrayAdapter<Integer> aa = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_dropdown_item,al);
		mWeeks.setAdapter(aa);
        int def = MusicUtils.getIntPref(this, "numweeks", 2); 
        int pos = icicle != null ? icicle.getInt("numweeks", def - 1) : def - 1;
        mWeeks.setSelection(pos);
        ((Button) findViewById(R.id.set)).setOnClickListener(mListener);
        ((Button) findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
    
    @Override
    public void onSaveInstanceState(Bundle outcicle) {
        outcicle.putInt("numweeks", mWeeks.getSelectedItemPosition());
    }
    
    @Override
    public void onResume() {
        super.onResume();
    }
    
    private View.OnClickListener mListener = new View.OnClickListener() {
        public void onClick(View v) {
            int numweeks = mWeeks.getSelectedItemPosition() + 1;
            MusicUtils.setIntPref(getBaseContext(), "numweeks", numweeks);
            setResult(RESULT_OK);
            finish();
        }
    };
}

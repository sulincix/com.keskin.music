package com.keskin.music;

import android.annotation.*;
import android.app.*;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.media.*;
import android.media.audiofx.*;
import android.os.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.android.music.*;
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import org.blinksd.utils.annonation.*;
import org.blinksd.utils.color.*;
import org.superdroid.db.*;

public class Additions {
	/*public static Bitmap fastblur(@NonNull Bitmap sentBitmap){
		
		try{
			float scale = 0.65f;
			int radius = 10;
			int width = Math.round(sentBitmap.getWidth() * scale);
			int height = Math.round(sentBitmap.getHeight() * scale);
			sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);
			Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
			if (radius < 1) return null;
			int w = bitmap.getWidth();
			int h = bitmap.getHeight();
			int[] pix = new int[w * h];
			bitmap.getPixels(pix, 0, w, 0, 0, w, h); 
			int wm = w - 1; 
			int hm = h - 1; 
			int wh = w * h; 
			int div = (radius * 2) + 1; 
			int r[] = new int[wh]; 
			int g[] = new int[wh]; 
			int b[] = new int[wh]; 
			int rsum, gsum, bsum, x, y, i, p, yp, yi, yw; 
			int vmin[] = new int[Math.max(w, h)]; 
			int divsum = (div + 1) >> 1;
			divsum *= divsum;
			int dv[] = new int[256 * divsum];
			for (i = 0; i < 256 * divsum; i++) dv[i] = (i / divsum);
			yw = yi = 0;
			int[][] stack = new int[div][3];
			int stackpointer,stackstart,rbs;
			int[] sir;
			int r1 = radius + 1;
			int routsum, goutsum, boutsum;
			int rinsum, ginsum, binsum;
			for (y = 0; y < h; y++){
				rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
				for (i = -radius; i <= radius; i++){
					p = pix[yi + Math.min(wm, Math.max(i, 0))];
					sir = stack[i + radius];
					sir[0] = (p & 0xff0000) >> 16;
					sir[1] = (p & 0x00ff00) >> 8;
					sir[2] = (p & 0x0000ff);
					rbs = r1 - Math.abs(i);
					rsum += sir[0] * rbs;
					gsum += sir[1] * rbs;
					bsum += sir[2] * rbs;
					if (i > 0){
						rinsum += sir[0];
						ginsum += sir[1];
						binsum += sir[2];
					} else {
						routsum += sir[0];
						goutsum += sir[1];
						boutsum += sir[2];
					}
				} stackpointer = radius;
				for (x = 0; x < w; x++){
					r[yi] = dv[rsum]; 
					g[yi] = dv[gsum];
					b[yi] = dv[bsum];
					rsum -= routsum;
					gsum -= goutsum;
					bsum -= boutsum;
					stackstart = stackpointer - radius + div;
					sir = stack[stackstart % div];
					routsum -= sir[0];
					goutsum -= sir[1];
					boutsum -= sir[2];
					if (y == 0) vmin[x] = Math.min(x + radius + 1, wm);
					p = pix[yw + vmin[x]];
					sir[0] = (p & 0xff0000) >> 16;
					sir[1] = (p & 0x00ff00) >> 8;
					sir[2] = (p & 0x0000ff);
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
					rsum += rinsum;
					gsum += ginsum;
					bsum += binsum;
					stackpointer = (stackpointer + 1) % div;
					sir = stack[stackpointer % div];
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
					rinsum -= sir[0];
					ginsum -= sir[1];
					binsum -= sir[2];
					yi++;
				} yw += w;
			}

			for (x = 0; x < w; x++) {
				rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
				yp = -radius * w;
				for (i = -radius; i <= radius; i++){
					yi = Math.max(0, yp) + x;
					sir = stack[i + radius];
					sir[0] = r[yi];
					sir[1] = g[yi];
					sir[2] = b[yi];
					rbs = r1 - Math.abs(i);
					rsum += r[yi] * rbs;
					gsum += g[yi] * rbs;
					bsum += b[yi] * rbs;
					if (i > 0){
						rinsum += sir[0];
						ginsum += sir[1];
						binsum += sir[2];
					} else {
						routsum += sir[0];
						goutsum += sir[1];
						boutsum += sir[2];
					}
					if (i < hm) yp += w;
				} yi = x;
				stackpointer = radius;
				for (y = 0; y < h; y++){
					pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];
					rsum -= routsum;
					gsum -= goutsum;
					bsum -= boutsum;
					stackstart = stackpointer - radius + div;
					sir = stack[stackstart % div];
					routsum -= sir[0];
					goutsum -= sir[1];
					boutsum -= sir[2];
					if (x == 0) vmin[y] = Math.min(y + r1, hm) * w;
					p = x + vmin[y];
					sir[0] = r[p];
					sir[1] = g[p];
					sir[2] = b[p];
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
					rsum += rinsum;
					gsum += ginsum;
					bsum += binsum;
					stackpointer = (stackpointer + 1) % div;
					sir = stack[stackpointer];
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
					rinsum -= sir[0];
					ginsum -= sir[1];
					binsum -= sir[2];
					yi += w;
				}
			} bitmap.setPixels(pix, 0, w, 0, 0, w, h);
			return bitmap;
		} catch(Exception e){
			return sentBitmap;
		}
	}*/
	
	public static int getAlbumArtColor(@NonNull Bitmap bitmap){
		if (bitmap == null) return 0;
		bitmap = Bitmap.createScaledBitmap(bitmap,64,64,true);
		int width = bitmap.getWidth(),height = bitmap.getHeight();
		int[] pixels = new int[width * height];
		bitmap.getPixels(pixels, 0, width, 1, 0, 1, height);
		int color,count = 0,r = 0,g = 0,b = 0,a = 0;
		for(int i = 0;i < pixels.length;i++){
			color = pixels[i];
			a = Color.alpha(color);
			if(a > 0){
				color = (a < 255) ? Color.rgb(Color.red(color),Color.green(color),Color.blue(color)) : color;
				if(color > 0xFF000000){
					r += Color.red(color);
					g += Color.green(color);
					b += Color.blue(color);
					count++;
				}
			}
		}
		if(r == g && g == b && r == 0){
			pixels = new int[width * 2];
			bitmap.getPixels(pixels, 0, width, 0, 0, width, 1);
			for(int i = 0;i < pixels.length;i++){
				color = pixels[i];
				a = Color.alpha(color);
				if(a > 0){
					color = (a < 255) ? Color.rgb(Color.red(color),Color.green(color),Color.blue(color)) : color;
					if(color > 0xFF000000){
						r += Color.red(color);
						g += Color.green(color);
						b += Color.blue(color);
						count++;
					}
				}
			}
		}
		if(r == g && g == b && r == 0){
			count = 1;
		}
		r /= count;
		g /= count;
		b /= count;
		r = (r << 16) & 0x00FF0000;
		g = (g << 8) & 0x0000FF00;
		b = b & 0x000000FF;
		color = 0xFF000000 | r | g | b;
		return color;
	}
	
	public static Drawable colorDrawable(@NonNull Drawable d, @ColorInt int color){
		d.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
		return d;
	}
	
	public static Bitmap colorBitmap(@NonNull Bitmap b, @ColorInt int color){
		Bitmap temp = b.copy(Bitmap.Config.ARGB_8888,true);
		Paint paint = new Paint();
		ColorFilter filter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP);
		paint.setColorFilter(filter);
		Canvas canvas = new Canvas(temp);
		canvas.drawBitmap(temp, 0, 0, paint);
		return temp;
	}
	
	public static Drawable createDrawable(int[] color){
		return createDrawable(color,0,new int[]{0,0});
	}
	
	public static Drawable createDrawable(int[] color,@FloatRange float r,int[] stroke){
		return createDrawable(color,r,stroke,GradientDrawable.Orientation.TOP_BOTTOM);
	}
	
	public static Drawable createDrawable(int[] color,@FloatRange float r,int[] stroke,GradientDrawable.Orientation ori){
		GradientDrawable gd = new GradientDrawable();
		gd.setColors(color);
		gd.setOrientation(ori);
		if(stroke != null && stroke.length > 0)
			gd.setStroke(stroke[0],stroke[1]);
		gd.setCornerRadius(r);
		return gd;
	}
	
	public static Drawable createDrawable(@IntRange int color,@FloatRange float r,int[] stroke){
		GradientDrawable gd = new GradientDrawable();
		gd.setColor(color);
		if(stroke != null && stroke.length > 0)
			gd.setStroke(stroke[0],stroke[1]);
		gd.setCornerRadius(r);
		return gd;
	}
	
	public static Bitmap circleBitmap(@NonNull Bitmap bitmap, boolean squared){
		if(squared) bitmap = centerBitmap(bitmap);
		Bitmap b = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(),Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		Paint p = new Paint();
		Rect r = new Rect(0,0,bitmap.getWidth(),bitmap.getHeight());
		p.setAntiAlias(true);
		p.setColor(0xFF424242);
		c.drawCircle(bitmap.getWidth()/2,bitmap.getHeight()/2,bitmap.getWidth()/2,p);
		p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		c.drawBitmap(bitmap,r,r,p);
		return b;
	}
	
	/*public static Bitmap setBitmapAlpha(Bitmap bitmap){
		Bitmap b = bitmap.copy(Bitmap.Config.ARGB_8888,true),
		d = Bitmap.createBitmap(b.getWidth(),b.getHeight(),Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(d);
		Paint p = new Paint();
		p.setColor(0xFF000000);
		c.drawCircle(b.getWidth()/2,b.getHeight()/2,b.getWidth()/2,p);
		c = new Canvas(b);
		p = new Paint();
		p.setAlpha(75);
		c.drawBitmap(d,0,0,p);
		circleBitmap(b,false);
		return b;
	}*/
	
	public static Bitmap resizeBitmap(@NonNull Bitmap bitmap, @FloatRange(from = -1) float w, @FloatRange(from = -1) float h){
		if(w == -1) w = bitmap.getWidth();
		if(h == -1) w = bitmap.getHeight();
		int ow = bitmap.getWidth(), oh = bitmap.getHeight();
		float sw = w / ow, sh = h / oh;
		Matrix m = new Matrix();
		m.postScale(sw,sh);
		return Bitmap.createBitmap(bitmap,0,0,ow,oh,m,false);
	}

	public static Bitmap centerBitmap(@NonNull Bitmap bitmap){
		int dimen = Math.min(bitmap.getWidth(),bitmap.getHeight());
		return cropBitmap(bitmap,dimen,dimen);
	}

	public static Bitmap cropBitmap(@NonNull Bitmap bitmap,@IntRange(from = -1) int w, @IntRange(from = -1) int h){
		if(w == -1) w = bitmap.getWidth();
		if(h == -1) w = bitmap.getHeight();
		bitmap = ThumbnailUtils.extractThumbnail(bitmap,w,h);
		return bitmap;
	}
	
	/*public static Bitmap createGradient(@ColorInt int color,@IntRange int w,@IntRange int h){
		Bitmap empty = Bitmap.createBitmap(256,256,Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(empty);
		Paint p = new Paint();
		p.setColor(color);
		p.setStrokeWidth(1);
		for(int i = 0;i < empty.getWidth();i++){
			c.drawLine(i,0,i,empty.getWidth(),p);
			p.setColor(p.getColor()-0x01000000);
		}
		return resizeBitmap(empty,w,h);
	}
	
	public static Bitmap drawSecondBitmapOverFirst(@NonNull Bitmap bmp1, @NonNull Bitmap bmp2){
		bmp1 = bmp1.copy(Bitmap.Config.ARGB_8888,true);
		bmp2 = bmp2.copy(Bitmap.Config.ARGB_8888,true);
		Canvas canvas = new Canvas(bmp1);
		float marginLeft = (float) (bmp1.getWidth() * 0.5 - bmp2.getWidth() * 0.5);
		float marginTop = (float) (bmp1.getHeight() * 0.5 - bmp2.getHeight() * 0.5);
		canvas.drawBitmap(bmp2, marginLeft, marginTop, new Paint());
		return bmp1;
    }*/
	/*public static SharedPreferences getSettings(@NonNull Context ctx){
		SharedPreferences reader = ctx.getSharedPreferences(ctx.getPackageName()+".settings",ctx.MODE_PRIVATE);
	    return reader;
	}*/
	/*public static Bitmap drawDisc(@NonNull Context ctx, @NonNull Bitmap source){
		
		if(getSettings(ctx).getBoolean("dvdcover",false)){
			Bitmap bmp = Bitmap.createBitmap((source.getWidth()/4)-1,(source.getHeight()/4)-1,Bitmap.Config.ARGB_8888);
			new Canvas(bmp).drawColor(0xFF987654);
			bmp = circleBitmap(bmp,true);
			source = drawSecondBitmapOverFirst(source,bmp);
			int[] pxs = new int[source.getWidth() * source.getHeight()];
			source.getPixels(pxs,0,source.getWidth(),0,0,source.getWidth(),source.getHeight());
			for(int i = 0;i != pxs.length;i++) if(pxs[i] == 0xFF987654) pxs[i] = 0;
			source.setPixels(pxs,0,source.getWidth(),0,0,source.getWidth(),source.getHeight());
			bmp = BitmapFactory.decodeResource(ctx.getResources(),R.drawable.disc1);
			bmp = resizeBitmap(bmp,source.getWidth()/4,source.getHeight()/4);
			source = drawSecondBitmapOverFirst(source,bmp);
		} 
		return source;
	}*/
	
	public static int getThemeColor(@NonNull Context c){
		if(Build.VERSION.SDK_INT > 19){
			TypedValue tv = new TypedValue();
			c.getTheme().resolveAttribute(android.R.attr.colorAccent,tv,true);
			return tv.data;
		} return 0xFF666666;
	}
	
	public static boolean detectNavbar(@NonNull Context context){
		if(Build.VERSION.SDK_INT >= 14){
			try {
				Class<?> serviceManager = Class.forName("android.os.ServiceManager");
				IBinder serviceBinder = (IBinder)serviceManager.getMethod("getService", String.class).invoke(serviceManager, "window");
				Class<?> stub = Class.forName("android.view.IWindowManager$Stub");
				Object windowManagerService = stub.getMethod("asInterface", IBinder.class).invoke(stub, serviceBinder);
				Method hasNavigationBar = windowManagerService.getClass().getMethod("hasNavigationBar");
				return (boolean) hasNavigationBar.invoke(windowManagerService);
			} catch(Exception e){
				return ViewConfiguration.get(context).hasPermanentMenuKey();
			}
		}
		return (!(KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK) && 
					KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME)));
	}
	
	public static void manageMenuView(@NonNull final Activity a, @NonNull HashMap<String,Drawable> hm, boolean open,final boolean invert){
		final ViewGroup vg = (ViewGroup) a.findViewById(R.id.menuLayout);
		final View iv = a.findViewById(R.id.albumSmall);
		
		if(open){
			vg.setOnClickListener(new View.OnClickListener(){
				public void onClick(View v){
					closeMenu(a);
				}
			});
			vg.removeAllViewsInLayout();
			String[] res1 = new String[hm.size()];
			Drawable[] res2 = new Drawable[hm.size()];
			int y = 0;
			Set<String> set = hm.keySet();
			set = new TreeSet<String>(set);
			for(String g : set){
				res1[y] = g.substring(1);
				res2[y] = hm.get(g);
				y++;
			}
			boolean b = a.getPackageManager().resolveActivity(
				new Intent(AudioEffect.ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL),0) != null;
			LinearLayout hl = null;
			int x;
			for(int i = 0;i != (x = res1.length + (b ? 0 : -1));){
				hl = hl(a.getBaseContext());
				for(int g = 0;g != 2;g++){
					if(i < x){
						hl.addView(menuItem(hl,res1[i],res2[i],getOcl(a,i),invert));
						i++;
					} /*else {
						hl.addView(menuItem(hl,res1[i-1],res2[i-1],null,false));
					}*/
				}
				vg.addView(hl);
			}
			vg.setVisibility(View.VISIBLE);
			iv.setVisibility(View.INVISIBLE);
			/*final MediaPlaybackActivity mpa = (MediaPlaybackActivity)a;
			mpa.mHandler.postDelayed(new Runnable(){
				public void run(){
					if(vg.getVisibility() == View.VISIBLE && 
						iv.getVisibility() == View.INVISIBLE){
						vg.setVisibility(View.INVISIBLE);
						iv.setVisibility(View.VISIBLE);
						mpa.menuOpened = false;
					}
				}
			},10000);*/
		} else {
			vg.setVisibility(View.INVISIBLE);
			iv.setVisibility(View.VISIBLE);
		}
	}
	
	private static LinearLayout hl(@NonNull Context c){
		LinearLayout ll = new LinearLayout(c);
		ll.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.MATCH_PARENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		ll.setGravity(Gravity.CENTER);
		return ll;
	}
	
	public static int getThemeWindowBackground(Context c){
		if(Build.VERSION.SDK_INT > 19){
			TypedValue tv = new TypedValue();
			c.getTheme().resolveAttribute(android.R.attr.windowBackground,tv,true);
			return tv.data;
		}
		return 0xFF212121;
	}
	
	public static void closeMenu(@NonNull final Activity a){
		if(a instanceof MediaPlaybackActivity)
			manageMenuView(a, null, (((MediaPlaybackActivity) a).menuOpened = false),false);
	}
	
	private static Intent intent;
	
	private static View.OnClickListener getOcl(@NonNull final Activity a, final int num){
		return new View.OnClickListener(){
			public void onClick(View v){
				closeMenu(a);
				IMediaPlaybackService mService = ((MediaPlaybackActivity) a).mService;
				switch(num){
					case 0:
						intent = new Intent(a.getBaseContext(), MusicBrowserActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
						a.startActivity(intent);
						a.finish();
						break;
					case 1:
						MusicUtils.togglePartyShuffle();
						((MediaPlaybackActivity)a).setShuffleButtonImage();
						break;
					case 2:
						if(mService != null){
							try {
								MusicUtils.setRingtone(a.getBaseContext(), mService.getAudioId());
							} catch(Exception e){}
						}
						break;
					case 3:
						if (mService != null) {
							try {
								long [] list = new long[1];
								list[0] = MusicUtils.getCurrentAudioId();
								Bundle b = new Bundle();
								String f;
								if (android.os.Environment.isExternalStorageRemovable()) {
									f = a.getString(R.string.delete_song_desc, mService.getTrackName());
								} else {
									f = a.getString(R.string.delete_song_desc_nosdcard, mService.getTrackName());
								}
								b.putString("description", f);
								b.putLongArray("items", list);
								intent = new Intent();
								intent.setClass(a.getBaseContext(), DeleteItems.class);
								intent.putExtras(b);
								a.startActivityForResult(intent, -1);
							} catch(Exception e){}
						}
						break;
					case 4:
						try {
							Dialog d = mediaInfoDialog(a,mService);
							d.show();
							//Toast.makeText(a,"Needs bug fixes",Toast.LENGTH_LONG).show();
						} catch(Exception e){
							Log.e("Additions",e.toString());
						}
						break;
					case 5:
						try {
							intent = new Intent(AudioEffect.ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL);
							intent.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, mService.getAudioSessionId());
							a.startActivityForResult(intent, MusicUtils.Defs.EFFECTS_PANEL);
						} catch(Exception e){}
						break;
					default:
						return;
				}
			}
		};
	}
	
	private static Dialog mediaInfoDialog(@NonNull Context c,@NonNull IMediaPlaybackService msc) throws Exception{
		AlertDialog.Builder ad = new AlertDialog.Builder(c);
		ad.setCancelable(true);
		//ad.setTitle(R.string.musicinfo);
		String s = "";
		try {
			s = c.getString(R.string.musicinfo_msg);
			/*s = s.replace("$1",msc.getTrackName());
			s = s.replace("$2",msc.getArtistName());
			s = s.replace("$3",msc.getAlbumName());
			/*File f = new File(msc.getRealPath());
			s = s.replace("$4",fileSizeCalculator(f.length()));
			s = s.replace("$5",f.toString());
			s = s.replace("$6",format("dd/MM/yyyy HH:mm:ss",f.lastModified()));
			s = s.replace("$7",msc.getMimeType());
			s = s.replace("$8",""+bitrateCalculator(c,f.length(),msc.duration()));*/
		} catch(Exception e){
			//throw e;
		}
		ad.setMessage(s);
		ad.setPositiveButton("×", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface p1, int p2){
				p1.dismiss();
			}
		});
		return ad.create();
	}
	
	private static String format(String pattern, long time){
		try {
			Class c = Class.forName("android.icu.text.SimpleDateFormat");
			if(c == null) throw new NoClassDefFoundError();
			Constructor cs = c.getConstructor(new Class[]{String.class});
			c = (Class) cs.newInstance(pattern);
			Method m = c.getMethod("format",new Class[]{long.class});
			m.setAccessible(true);
			return (String) m.invoke(time);
		} catch(Exception | Error e){
			return new java.text.SimpleDateFormat(pattern).format(time);
		}
	}
	
	private static long bitrateCalculator(@NonNull Context c, long fileSize,long duration){
		String s = MusicUtils.makeTimeString(c,duration / 1000);
		String[] a = s.split(":");
		duration = (Integer.parseInt(a[0]) * 60) + Integer.parseInt(a[1]);
		duration *= 100;
		duration = (fileSize / duration);
		fileSize = (32 * (duration / 128));
		return duration - fileSize;
	}
	
	public static String fileSizeCalculator(long size){
		return (size/1000)+" KB";
	}
	
	private static LinearLayout menuItem(@NonNull View v, String s, @NonNull Drawable res, View.OnClickListener ocl, boolean invert){
		LinearLayout ll = new LinearLayout(v.getContext());
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
											LinearLayout.LayoutParams.MATCH_PARENT,
											LinearLayout.LayoutParams.WRAP_CONTENT);
		ll.setLayoutParams(lp);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setOnClickListener(ocl);
		ll.setGravity(Gravity.CENTER);
		TextView tv = new TextView(v.getContext());
		tv.setLayoutParams(new LinearLayout.LayoutParams(
							   LinearLayout.LayoutParams.MATCH_PARENT,
							   LinearLayout.LayoutParams.WRAP_CONTENT));
		tv.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
		tv.setText(s);
		tv.setGravity(Gravity.CENTER_HORIZONTAL);
		tv.setTextColor(0xCCFFFFFF - (invert ? 0x00FFFFFF : 0));
		tv.setSingleLine();
		tv.setEllipsize(TextUtils.TruncateAt.MARQUEE);
		tv.setMarqueeRepeatLimit((int)Math.pow(2,32));
		tv.setSelected(true);
		ImageView iv = new ImageView(v.getContext());
		LinearLayout.LayoutParams ip = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.MATCH_PARENT,
			LinearLayout.LayoutParams.WRAP_CONTENT);
		iv.setLayoutParams(ip);
		int i = ((int) tv.getTextSize());
		tv.setPadding(i/2,0,i/2,0);
		lp.leftMargin = lp.rightMargin = i/2;
		ip.width = ip.height = i*5;
		iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
		iv.setPadding(i/2,i/2,i/2,i/2);
		iv.setImageDrawable(Additions.colorDrawable(res,invert ? 0xFF000000 : 0xFFFFFFFF));
		ll.setBackground(createDrawable(0x22FFFFFF - (invert ? 0x00FFFFFF : 0),i,new int[]{i/8,0x66FFFFFF - (invert ? 0x00FFFFFF : 0)}));
		ll.setPadding(i/2,i/2,i/2,i/2);
		lp.width = i*10;
		lp.height = i*8;
		ll.addView(iv);
		ll.addView(tv);
		LinearLayout.LayoutParams vp = (LinearLayout.LayoutParams) v.getLayoutParams();
		vp.bottomMargin = vp.topMargin = i/2;
		return ll;
	}
	
	public static Bitmap drawBack(){
		Bitmap b = Bitmap.createBitmap(100,100,Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		Paint p = new Paint();
		p.setStrokeWidth(10);
		p.setStrokeCap(Paint.Cap.ROUND);
		p.setStrokeJoin(Paint.Join.ROUND);
		p.setColor(0xFF212121);
		c.drawLine(60,10,30,44,p);
		c.drawLine(30,46,60,80,p);
		c.drawPoint(29,45,p);
		return b;
	}
	
	public static void audioFXBugFix(@NonNull Context c){
		if(/*c.getSharedPreferences("Music",c.MODE_PRIVATE)*/
			SuperDBHelper.getDefault(c).getBoolean("audfix",false)){
			try {
				//c.sendBroadcast(new Intent(MediaPlaybackService.PAUSE_ACTION));
				java.lang.Process p = Runtime.getRuntime().exec("su");
				DataOutputStream dos = new DataOutputStream(p.getOutputStream());
				dos.writeBytes("killall audioserver\n");
				dos.flush();
				dos.close();
				//c.sendBroadcast(new Intent(MediaPlaybackService.TOGGLEPAUSE_ACTION));
			} catch(Exception | Error e){}
		}
	}
	
	public static int dp(@IntRange(from = 0) int px){
		return (int)(Resources.getSystem().getDisplayMetrics().density * px);
	}
	
	public static boolean itemsNeedInversion(@ColorInt int color1){
		return ColorUtils.isColorLight(color1);
	}
	
	public static Bitmap drawDrawableOverBitmap(Drawable d, Bitmap source){
		source = source.copy(Bitmap.Config.ARGB_8888,true);
		Canvas c = new Canvas(source);
		d.setBounds(0,0,source.getWidth(),source.getHeight());
		d.draw(c);
		return source;
	}
	
	public static final String CHANNEL_ID = "nowPlaying";
	
	@TargetApi(26)
	public static void setNotificationChannel(Service s){
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
			String name = s.getString(R.string.nowplaying_title);
			NotificationManager nm = (NotificationManager) s.getSystemService(Context.NOTIFICATION_SERVICE);
			NotificationChannel nc = new NotificationChannel(CHANNEL_ID,name,NotificationManager.IMPORTANCE_DEFAULT);
			nc.setSound(null,null);
			nc.setShowBadge(false);
			nc.enableVibration(false);
			nc.enableLights(false);
			nc.setVibrationPattern(new long[]{0,0,0});
			nm.createNotificationChannel(nc);
		}
	}
	
	public static Bitmap drawableAsBitmap(Drawable d, int w, int h){
		Bitmap b = Bitmap.createBitmap(w,h,Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		d.setBounds(0,0,w,h);
		d.draw(c);
		return b;
	}
	
	/*public static Bitmap changeSpecificColor(@NonNull Bitmap source,@ColorInt int color,@ColorInt int target){
		source = source.copy(Bitmap.Config.ARGB_8888,true);
		int[] pxs = new int[source.getWidth() * source.getHeight()];
		source.getPixels(pxs,0,source.getWidth(),0,0,source.getWidth(),source.getHeight());
		for(int i = 0;i != pxs.length;i++) if(pxs[i] == color) pxs[i] = target;
		source.setPixels(pxs,0,source.getWidth(),0,0,source.getWidth(),source.getHeight());
		return source;
	}*/
	
}

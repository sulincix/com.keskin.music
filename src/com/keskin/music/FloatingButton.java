package com.keskin.music;

import android.app.*;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.net.*;
import android.os.*;
import android.provider.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import static android.view.Gravity.*;

import com.android.music.*;

public class FloatingButton extends LinearLayout {

    private static int BUTTON_SIZE = 64, DEFAULT_ICON_COLOR = 0xFFFFFFFF, clr, iclr = DEFAULT_ICON_COLOR;
    private static boolean REVERSE = false, EXCEPTION = false, OLD_REVERSE = false;
    private LinearLayout btns = null;
    private ImageView main = null;
    private ViewGroup sv = null;
    private View bf = null, va[] = null;
    private Orientation oldOri = null;
	public static final String UPDATE_BUTTON = "com.keskin.music.UPDATE_BUTTON";

    public FloatingButton(Context c,AttributeSet attributeSet){
        super(c);
        setLayoutParams(new LayoutParams(-1,-1));
        clr = getAccentColorOrDefault();
        setOrientation(Orientation.BRV);
        addButton(SubButton.newButton(c.getResources(),android.R.drawable.ic_input_add,null));
        addButton(SubButton.newButton(c.getResources(),R.drawable.ic_tab_artists,new OnButtonClickListener("vnd.android.cursor.dir/artistalbum")));
        addButton(SubButton.newButton(c.getResources(),R.drawable.ic_tab_albums,new OnButtonClickListener("vnd.android.cursor.dir/album")));
        addButton(SubButton.newButton(c.getResources(),R.drawable.ic_audiotrack_dark,new OnButtonClickListener("vnd.android.cursor.dir/track")));
        addButton(SubButton.newButton(c.getResources(),R.drawable.ic_tab_playlists,new OnButtonClickListener("vnd.android.cursor.dir/playlist")));
        //MediaStore.Audio.Playlists.CONTENT_TYPE
        addButton(SubButton.newButton(c.getResources(),R.drawable.ic_tab_settings,new OnButtonClickListener("settings")));
		IntentFilter f = new IntentFilter(UPDATE_BUTTON);
        getContext().registerReceiver(playChangeListener,f);
    }
	
	private BroadcastReceiver playChangeListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent){
			try {
				int c = intent.getIntExtra(UPDATE_BUTTON,getAccentColorOrDefault());
				setButtonBackgroundColor(c);
				setButtonItemColor(Additions.itemsNeedInversion(c) ? 0xFF212121 : 0xFFFFFFFF);
			} catch(Throwable e){
				Toast.makeText(context,e.toString(),Toast.LENGTH_LONG).show();
			}
        }
    };

    @Override
    public int getOrientation(){
        if(EXCEPTION){
            setOrientation(-100);
            return -1;
        }
        EXCEPTION = true;
        return super.getOrientation();
    }

    public Orientation getFBOrientation(){
        return oldOri;
    }

    public void setOrientation(Orientation ori){
        if(ori != oldOri){
            EXCEPTION = false;
            REVERSE = false;
            switch(ori){
                case BLV:
                    setGravity(BOTTOM | LEFT);
                    setOrientation(VERTICAL);
                    REVERSE = true;
                    break;
                case BLH:
                    setGravity(BOTTOM | LEFT);
                    setOrientation(HORIZONTAL);
                    break;
                case BRV:
                    setGravity(BOTTOM | RIGHT);
                    setOrientation(VERTICAL);
                    REVERSE = true;
                    break;
                case BRH:
                    setGravity(BOTTOM | RIGHT);
                    setOrientation(HORIZONTAL);
                    REVERSE = true;
                    break;
                case TLV:
                    setGravity(TOP | LEFT);
                    setOrientation(VERTICAL);
                    break;
                case TLH:
                    setGravity(TOP | LEFT);
                    setOrientation(HORIZONTAL);
                    break;
                case TRV:
                    setGravity(TOP | RIGHT);
                    setOrientation(VERTICAL);
                    break;
                case TRH:
                    setGravity(TOP | RIGHT);
                    setOrientation(HORIZONTAL);
                    REVERSE = true;
                    break;
            }
            if(btns != null){
                btns.setOrientation(getOrientation());
                va = new View[btns.getChildCount()];
                for(int i = 0;i < va.length;i++){
                    va[i] = btns.getChildAt(i);
                }
                setScrollView();
                for(int i = 0;i < va.length;i++){
                    add(btns,va[i],OLD_REVERSE);
                }
                if(REVERSE){
                    addView(bf);
                    addView(sv);
                    addView(main);
                } else {
                    addView(main);
                    addView(sv);
                    addView(bf);
                }
                va = null;
            }
            EXCEPTION = true;
            OLD_REVERSE = REVERSE;
            oldOri = ori;
        }
    }

    public void addButton(Resources r, int res, OnButtonClickListener ocl){
        addButton(SubButton.newButton(r,res,ocl));
    }

    public void addButton(int res, OnButtonClickListener ocl){
        addButton(SubButton.newButton(res,ocl));
    }

    public void addButton(Drawable icon, OnButtonClickListener ocl){
        addButton(SubButton.newButton(icon,ocl));
    }

    public void addButton(SubButton sb){
        ImageView b = new ImageView(getContext());
        b.setImageDrawable(sb.i);
        int p = dp(16);
        b.setPadding(p,p,p,p);
        b.setBackgroundDrawable(getButtonBackground());
        b.setScaleType(ImageView.ScaleType.FIT_CENTER);
        b.setOnClickListener(sb.o != null ? sb.o : new OnButtonClickListener(""));
        if(getChildCount() != 0){
            b.setScaleX(0);
            b.setScaleY(0);
        }
        p = dp(BUTTON_SIZE);
        if(btns == null){
            addView(main = b);
            b.setTag(getChildCount());
            b.setLayoutParams(new LayoutParams(p,p,0));
            btns = new LinearLayout(getContext());
            EXCEPTION = false;
            btns.setOrientation(getOrientation());
            btns.setLayoutParams(new ScrollView.LayoutParams(-2,-2));
            setScrollView();
            add(this,sv);
            // BUG FIX ITEM, DON'T DELETE IT
            bf = new View(getContext());
            bf.setLayoutParams(new LayoutParams(p,p,0));
            add(this,bf);
        } else {
            b.setLayoutParams(new LayoutParams(p,p));
            b.setTag(btns.getChildCount());
            add(btns,b);
        }
    }

    private void setScrollView(){
        if(sv != null){
            btns.removeAllViewsInLayout();
            sv.removeAllViewsInLayout();
            removeAllViewsInLayout();
        }
        sv = (btns.getOrientation() == VERTICAL)
                ? new ScrollView(getContext())
                : new HorizontalScrollView(getContext());
        sv.setLayoutParams(new LayoutParams(-2,-2,0));
        sv.setVisibility(View.GONE);
        sv.addView(btns);
        sv.setHorizontalScrollBarEnabled(false);
        sv.setVerticalScrollBarEnabled(false);
    }

    @Override
    public void setOrientation(int orientation){
        if(EXCEPTION){
            throw new RuntimeException("Incompatible method for "+getClass().getSimpleName());
        } else {
            super.setOrientation(orientation);
        }
    }

    private int getAccentColorOrDefault(){
        if(Build.VERSION.SDK_INT > 20){
            TypedValue typedValue = new TypedValue();
            getContext().getTheme().resolveAttribute(android.R.attr.colorAccent, typedValue, true);
            return typedValue.data;
        }
        return 0xFF6699FF;
    }
	
	Drawable buttonBg = null;

    private Drawable getButtonBackground(){
		if(buttonBg == null){
			GradientDrawable gd = new GradientDrawable();
			gd.setShape(GradientDrawable.OVAL);
			gd.setStroke(dp(12),0,0,0);
			gd.setColor(clr);
			GradientDrawable ld = new GradientDrawable();
			ld.setShape(GradientDrawable.OVAL);
			ld.setStroke(dp(8),0,0,0);
			ld.setColor(clr - 0xBB000000);
			buttonBg = new LayerDrawable(new Drawable[]{ld,gd});
		}
        return buttonBg;
    }

    private void add(ViewGroup g, View v){
        add(g,v,REVERSE);
    }

    private void add(ViewGroup g, View v, boolean force){
        if(force){
            g.addView(v,0);
        } else {
            g.addView(v);
        }
    }

    private static int dp(int px){
        return (int)(Resources.getSystem().getDisplayMetrics().density * px);
    }

    public class OnButtonClickListener implements OnClickListener {
		
        int baseDelay = 25;
        String type="";

        public OnButtonClickListener(String t) {
            type=t;
        }

        @Override
        public void onClick(View v){
            if(getChildCount() != 1){
                if(btns.getChildAt(0).getScaleX() == 0){
                    sv.setVisibility(VISIBLE);
                    main.animate().rotation(135);
                } else {
                    main.animate().rotation(0);
                }
                for(int i = 0;i < btns.getChildCount();i++){
                    final int g = i,d1 = Math.abs((btns.getChildCount()-1)-(i+1))*baseDelay,d2 = (i+1)*baseDelay;
                    if(btns.getChildAt(0).getScaleX() == 0){
                        btns.getChildAt(i).animate().scaleX(1).scaleY(1).setStartDelay(REVERSE ? d1 : d2).withStartAction(new Runnable(){
                            public void run(){
                                btns.getChildAt(g).setVisibility(View.VISIBLE);
                                if(g == (REVERSE ? (btns.getChildCount()-1) : 0)){
                                    if(REVERSE){
                                        sv.setScrollX(btns.getChildCount()*dp(BUTTON_SIZE));
                                        sv.setScrollY(btns.getChildCount()*dp(BUTTON_SIZE));
                                    } else {
                                        sv.setScrollX(0);
                                        sv.setScrollY(0);
                                    }
                                }
                            }
                        });
                    } else {
                        btns.getChildAt(i).animate().scaleX(0).scaleY(0).setStartDelay(REVERSE ? d2 : d1).withEndAction(new Runnable(){
                            public void run(){
                                btns.getChildAt(g).setVisibility(View.INVISIBLE);
                                if(g == (REVERSE ? (btns.getChildCount()-1) : 0)){
                                    sv.setVisibility(GONE);
                                }
                            }
                        });
                    }
                }
            }
            if(type.equals("settings")){
                Intent intent = new Intent(getContext(),Settings.class);
                getContext().startActivity(intent);
            } else if(!type.equals("")){
				((Activity)getContext()).overridePendingTransition(0,0);
				((Activity)getContext()).finish();
				((Activity)getContext()).overridePendingTransition(0,0);
				Intent intent = null;
				switch(type){
					case "vnd.android.cursor.dir/artistalbum":
						intent = new Intent(v.getContext(),ArtistAlbumBrowserActivity.class);
						break;
					case "vnd.android.cursor.dir/album":
						intent = new Intent(v.getContext(),AlbumBrowserActivity.class);
						break;
					case "vnd.android.cursor.dir/track":
						intent = new Intent(v.getContext(),TrackBrowserActivity.class);
						break;
					case "vnd.android.cursor.dir/playlist":
						intent = new Intent(v.getContext(),PlaylistBrowserActivity.class);
						break;
					default:
						intent = new Intent(Intent.ACTION_PICK);
						intent.setDataAndType(Uri.EMPTY, type);
						break;
				}
				intent.putExtra("withtabs", true);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getContext().startActivity(intent);
				((Activity)getContext()).overridePendingTransition(0,0);
            }
        }
    }
	
	public void setButtonBackgroundColor(int bgColor){
		clr = bgColor;
		main.getDrawable().setColorFilter(bgColor,PorterDuff.Mode.SRC_ATOP);
		buttonBg.setColorFilter(bgColor,PorterDuff.Mode.SRC_ATOP);
	}
	
	public void setButtonItemColor(int itemColor){
		iclr = itemColor;
		main.getDrawable().setColorFilter(itemColor,PorterDuff.Mode.SRC_ATOP);
		for(int i = 0;i < btns.getChildCount();i++){
			ImageView v = (ImageView) btns.getChildAt(i);
			Drawable d = v.getDrawable();
			if(d != null){
				d.setColorFilter(itemColor,PorterDuff.Mode.SRC_ATOP);
			}
		}
	}

    public static class SubButton {

        private SubButton(){}

        private static SubButton s = null;
        private static Drawable i = null;
        private static OnButtonClickListener o = null;

        public static SubButton newButton(Drawable img, OnButtonClickListener ocl){
            s.i = img;
            s.i.setColorFilter(iclr,PorterDuff.Mode.SRC_ATOP);
            s.o = ocl;
            return s;
        }

        public static SubButton newButton(int res, OnButtonClickListener ocl){
            return newButton(Resources.getSystem().getDrawable(res),ocl);
        }

        public static SubButton newButton(Resources r, int res, OnButtonClickListener ocl){
            return newButton(r.getDrawable(res),ocl);
        }

    }

    public static enum Orientation { BRH, BRV, BLH, BLV, TRH, TRV, TLH, TLV }
}
